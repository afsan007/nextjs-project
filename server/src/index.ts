import http from 'http';
import dotEnv from 'dotenv';
import express from 'express';
import { ApolloServer } from 'apollo-server-express';

import { PicsProvider, typeDefs, resolvers } from './pics';

dotEnv.config();

export interface Context {
  dataSources: {
    PicsProvider: PicsProvider;
  };
}

const dataSources = (): Context['dataSources'] => {
  return {
    PicsProvider: new PicsProvider()
  };
};
const app = express();
const httpServer = http.createServer(app);

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources
});

server.applyMiddleware({ app, cors: true, path: '/api/graphql' });
server.installSubscriptionHandlers(httpServer);

httpServer.listen(process.env.PORT, () => {
  console.log(
    `🚀Server ready at http://localhost:${process.env.PORT}${server.graphqlPath}`
  );
});
