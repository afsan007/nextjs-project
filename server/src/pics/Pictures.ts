import { DataSource } from 'apollo-datasource';
import { pics } from './data';
import { Pic, QueryPicArgs } from '../types/graphql';

// This is a (simple) data source which can be used for retrieving
// the sample collection of books. This dataSource is injected
// into the context of the apollo server, which makes it usable
// inside the resolvers.
export class PicsProvider extends DataSource {
  public async getPic(args: QueryPicArgs): Promise<Pic> {
    return pics[args.id];
  }

  async getPics(): Promise<Pic[]> {
    return pics;
  }

  async AddPics(title: string, author: string): Promise<Pic> {
    console.log(title);
    pics.push({ id: 123, title, author });
    console.log(pics);
    return pics[2];
  }
}
