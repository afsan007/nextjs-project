export { pics } from './data';
export { PicsProvider } from './Pictures';
export { resolvers } from './resolvers';
export { typeDefs } from './typeDefs';
