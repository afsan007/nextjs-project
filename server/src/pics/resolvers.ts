import { QueryPicArgs, Pic } from '../types/graphql';
import { Context } from '../index';
import { GraphQLScalarType, Kind } from 'graphql';

export const resolvers = {
  Result: {
    __resolveType(obj, _, __) {
      if (obj.name) {
        return 'Book';
      }

      if (obj.title) {
        return 'Pic';
      }

      return null;
    }
  },
  Date: new GraphQLScalarType({
    name: 'Date',
    description: 'Date custom scalar type',
    parseValue(value) {
      return new Date(value); // value from the client
    },
    serialize(value) {
      return value.getTime(); // value sent to the client
    },
    parseLiteral(ast) {
      if (ast.kind === Kind.INT) {
        return parseInt(ast.value, 10); // ast value is always in string format
      }
      return null;
    }
  }),
  Query: {
    pic: (_, args: QueryPicArgs, ctx: Context): Promise<Pic> =>
      ctx.dataSources.PicsProvider.getPic(args),
    pics: (_, __, ctx: Context): Promise<Pic[]> =>
      ctx.dataSources.PicsProvider.getPics()
  },
  Mutation: {
    addPic: (_, args, ctx: Context): Promise<Pic> => {
      return ctx.dataSources.PicsProvider.AddPics(
        args.pic.title,
        args.pic.author
      );
    }
  }
};
