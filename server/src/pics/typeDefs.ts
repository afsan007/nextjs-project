import { gql } from 'apollo-server-express';
// which ways the data can be fetched from the GraphQL server.
export const typeDefs = gql`
  scalar Date
  union Result = Pic | Book
  enum AllowedStatus {
    DONE
    EXTRACT
    PROCESS
  }
  # scalar MyCustomScalar
  interface MutationResponse {
    code: String!
    success: Boolean!
    message: String!
    status: AllowedStatus!
  }

  # This "Pic" type can be used in other type declarations.
  type Pic implements MutationResponse {
    code: String!
    success: Boolean!
    message: String!
    title: String!
    author: String!
    status: AllowedStatus!
    date: Date!
  }
  type Book {
    name: String!
  }
  # The "Query" type is the root of all GraphQL queries.
  type Query {
    pic(id: Int!): Pic
    pics: [Pic]
  }

  type Mutation {
    addPic(pic: TypePicInput): Pic
  }
  input TypePicInput {
    "the name of pic"
    title: String
    "the owner of pic"
    author: String
  }
`;
