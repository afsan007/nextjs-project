export const pics = [
  {
    id: 0,
    title: 'Harry Potter and the Chamber of Secrets',
    author: 'J.K. Rowling'
  },
  {
    id: 1,
    title: 'Jurassic Park',
    author: 'Michael Crichton'
  }
];
