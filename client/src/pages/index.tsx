import { GetStaticProps } from 'next'
import Head from 'next/head'
export interface Props<T> {}

export default function Home() {
	return (
		<div className='container'>
			<Head>
				<title>InstagramApp</title>
				<link rel='icon' href='/favicon.ico' />
			</Head>
		</div>
	)
}

export const getStaticProps: GetStaticProps = async ctx => {
	return { props: { param: 1 } }
}
