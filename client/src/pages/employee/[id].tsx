import { GetStaticPaths, GetStaticProps } from 'next'
import fetch from 'node-fetch'

import { Employee } from '../../../model/employee'
export interface Props<T> {
	employee: T
	status: string
}
console.log('==> Author ==>', process.env.AUTHOR)
export default function ({ employee }: Props<Employee>) {
	console.log(
		'==> node ==>',
		process.env.NEXT_PUBLIC_ANALYTICS_ID
	)
	if (!employee) return <div>Loading...</div>
	return (
		<fieldset>
			<div>{employee.name}</div>
			<div>{employee.gender}</div>
			<div>{employee.email}</div>
			<div>{employee.age}</div>
			<div>{employee.isActive}</div>
		</fieldset>
	)
}

export const getStaticProps: GetStaticProps = async ctx => {
	const res = await fetch(
		'http://www.json-generator.com/api/json/get/cfRPzmjsmW?indent=2'
	)
	let employee: Employee[] = await res.json()
	employee = employee.filter(
		(el, index) => index.toString() === ctx.params.id
	)
	return { props: { employee: employee[0], status: 'ok' } }
}

export const getStaticPaths: GetStaticPaths = async () => {
	return {
		paths: [{ params: { id: '1' } }, { params: { id: '2' } }],
		fallback: true,
	}
}
