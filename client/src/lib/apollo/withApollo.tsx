import { ApolloProvider } from '@apollo/react-hooks'
import { createHttpLink } from 'apollo-link-http'
import {
	InMemoryCache,
	NormalizedCacheObject,
} from 'apollo-cache-inmemory'
import { ApolloClient } from 'apollo-client'

type CreateApolloClient = () => ApolloClient<
	NormalizedCacheObject
>
const createApolloClient: CreateApolloClient = () => {
	return new ApolloClient({
		ssrMode: typeof window === 'undefined',
		link: createHttpLink({
			uri: `${process.env.SERVER_ADDRESS}/graphql`,
		}),
		cache: new InMemoryCache(),
	})
}

let apolloClient: ApolloClient<any>

/**
 * Always creates a new apollo client on the server
 * Creates or reuses apollo client in the browser.
 * @returns apollo client
 */
const getApolloClient: () => ApolloClient<any> = () => {
	// Make sure to create a new client for every server-side request so that data
	// isn't shared between connections (which would be bad)
	if (typeof window === 'undefined') {
		return createApolloClient()
	}

	// Reuse client on the client-side
	if (!apolloClient) {
		apolloClient = createApolloClient()
	}

	return apolloClient
}

export default PageComponent => props => (
	<ApolloProvider client={getApolloClient()}>
		<PageComponent {...props} />
	</ApolloProvider>
)
