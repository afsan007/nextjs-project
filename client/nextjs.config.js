require('dotenv').configure()
module.exports = {
	env: {
		AUTHOR: process.env.AUTHOR,
	},
	serverRuntimeConfig: {
		// Will only be available on the server side
		secondSecret: process.env.AUTHOR, // Pass through env variables
	},
	publicRuntimeConfig: {
		// Will be available on both server and client
		staticFolder: '/static',
	},
}
