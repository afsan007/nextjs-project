export interface IndexProps {
	params: {
		status: string
		data: [
			{
				id: string
				employer_name: string
				employee_salary: string
				employee_age: string
				profile_image: string
			}
		]
	}
}
export interface Employee {
	id: string
	picture: string
	balance: string
	name: string
	phone: string
	gender: string
	age: number
	registered: string
	longitude: number
	_id: string
	eyeColor: string
	email: string
	favoriteFruit: string
	latitude: number
	guid: string
	company: string
	isActive: boolean
}
